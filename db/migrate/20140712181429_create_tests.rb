class CreateTests < ActiveRecord::Migration
  def change
    create_table :tests do |t|
      t.string :bar
      t.string :bur

      t.timestamps
    end
  end
end
